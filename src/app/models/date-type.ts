export class DateType {
  constructor(_name: string, _age: number, _symptoms: string, _date: string, _time: string) {
    this.name = _name;
    this.age = _age;
    this.symptoms = _symptoms;
    this.date = _date;
    this.time = _time;
  }

  name: string;
  age: number;
  symptoms: string;
  date: string;
  time: string;
}
