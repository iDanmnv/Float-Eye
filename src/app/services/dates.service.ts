import { Injectable } from '@angular/core';
import { DateType } from '../models/date-type';

@Injectable({
  providedIn: 'root'
})
export class DatesService {
  currentDate: DateType = new DateType("", 0, "", "", "");
  dates: DateType[] = [];

  constructor() {
    this.dates.push(new DateType("Daniel Molina", 21, "Dolor de cabeza", "2019-05-13", "17:00"));
    this.dates.push(new DateType("Carolina Viques", 20, "Dolor de garganta", "2019-05-13", "18:00"));
    this.dates.push(new DateType("Omar Salazar", 21, "Dolor de piernas", "2019-05-14", "18:00"));
    this.dates.push(new DateType("Erick Meza", 20, "Dolor de manos", "2019-05-14", "19:00"));
  }

  getDates(): any {
    return this.dates;
  }

  addDate(_date: DateType): void {
    this.dates.push(_date);
    this.dates = this.dates.sort((date1, date2) => {
      return (date1.date < date2.date)? 0 : 1;
    });
  }

  length(): number {
    return this.dates.length;
  }
}
