import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import { Location } from '@angular/common';
import { DatesService } from 'src/app/services/dates.service';
import { DateType } from 'src/app/models/date-type';

declare let $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild("modal") private modalRef: ElementRef;

  title: string;
  tempDate: Date;
  form: FormGroup;
  message: string;
  currentDate: string;
  pass: boolean;
  init: boolean [] = [true, true, true, true, true];

  constructor(private dateService: DatesService, private location: Location) {
    this.form = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*')]),
      'age': new FormControl('', [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$'), Validators.min(1), Validators.max(90)]),
      'symptoms': new FormControl('', Validators.required),
      'date': new FormControl('', Validators.required),
      'time': new FormControl('',Validators.required)
    })

    this.tempDate = new Date();
    let month: string = (this.tempDate.getMonth() + 1).toString();
    let day: string = (this.tempDate.getDate()).toString();
    if ((this.tempDate.getMonth() + 1) < 10)
      month = "0" + month;
    if ((this.tempDate.getDate()) < 10)
      day = "0" + day;

    this.currentDate = this.tempDate.getFullYear() + "-" + month + "-" + day;
  }

  ngOnInit() {
    
  }

  goBack(): void {
    this.location.back();
  }

  getDetection(input: number): void {
    if (this.init[input] == true)
      this.init[input] = false;
  }
  
  pushDate(): void {
    let _name: string = this.form.value.name;
    let _age: number = this.form.value.age;
    let _date: string = this.form.value.date;
    let _time: string = this.form.value.time;
    let _symptoms: string = this.form.value.symptoms;
    let object: DateType = new DateType(_name, _age, _symptoms, _date, _time);
    this.pass = true;
    this.dateService.dates.forEach(date => {
      if (_date == date.date && _time == date.time)
        this.pass = false;
    });
    if (this.pass) {
      this.title = "Cita registrada";
      this.message = "Datos de la cita:";
      this.dateService.addDate(object);
      this.dateService.currentDate = object;
      this.form.reset();
    }
    else {
      this.title = "Error";
      this.message = "Ya existe una cita en esta fecha y hora, puedes ver la disponibilidad de fechas en el apartado de 'Agenda', después intenta con otra fecha/hora."
    }
    $(this.modalRef.nativeElement).modal('show');
    for (const index in this.init)
      this.init[index] = true;
  }
}
