import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DatesService } from 'src/app/services/dates.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  constructor(private location: Location, private dateService: DatesService) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

}
